# Snark prototype

## How to use

```shell
python3 Snark.py <root.md> <output.html>
```
where:
1) <root.md> - root of your markdown hierarchy (example/root.md by default)
2) <output.html> - output file final html-file (example/output.html by default)