import subprocess
import json
import pathlib
import sys

def main():
    root_path = sys.argv[1] if len(sys.argv) >= 2 else "example/root.md"
    output_path = sys.argv[2] if len(sys.argv) >= 3 else "example/output.html"

    root_ast = getMdAst(root_path)
    resolveDependencies(root_ast, root_path)

    with open(output_path, "w+") as file:
        file.write(getHtml(json.dumps(root_ast)))
    
    return 0

def getMdAst(file_name):
    return json.loads(subprocess.run( \
            ["node", "MarkdownParser.js", file_name], capture_output = True, text = True).stdout)

def resolveDependencies(md_ast, path):
    if ("children" in md_ast):
        children_to_be_replaced = []
        replacements_lists = []

        for child in md_ast["children"]:
            if (child["type"] == "paragraph"):
                assert("children" in child)
                assert(len(child["children"]) == 1)

                if (child["children"][0]["type"] == "text"):
                    include_list = getIncludeFiles(child["children"][0]["value"])
                    if (len(include_list) > 0):
                        children_to_be_replaced.append(child)                       
                        replacements_lists.append(getInsidesOfIncludedFiles(include_list, path))
            else:
                resolveDependencies(child, path)

        for child_to_be_replaced, replacement in zip(children_to_be_replaced, replacements_lists):
            index = md_ast["children"].index(child_to_be_replaced)
            del md_ast["children"][index]
            md_ast["children"][index : index] = replacement

def getIncludeFiles(string):
    return json.loads(subprocess.run(["python3", "SnarkParser.py", string], \
            check = True, capture_output = True, text = True).stdout)

def getInsidesOfIncludedFiles(include_list, path):
    local_folder_path = str(pathlib.Path(path).parent)
    include_paths = [local_folder_path + "/" + file_name for file_name in include_list]

    md_asts_to_include = list(map(lambda path: getMdAst(path), include_paths))

    for md_ast_to_include, include_path in zip(md_asts_to_include, include_paths):
        resolveDependencies(md_ast_to_include, include_path)

    for index in range(len(md_asts_to_include)):
        md_asts_to_include[index] = deleteRoot(md_asts_to_include[index])

    return [item for sublist in md_asts_to_include for item in sublist]

def deleteRoot(md_ast):
    assert(md_ast["type"] == "root")
    return md_ast["children"]

def getHtml(json_string):
    return subprocess.run( \
            ["node", "HtmlRenderer.js", json_string], capture_output = True, text = True).stdout

if __name__ == "__main__":
    main()