import {readFileSync} from 'fs'
import {fromMarkdown} from 'mdast-util-from-markdown'

main()

function main()
{
    const markdown_file = process.argv.length >= 3 ? process.argv[2] : "example/root.md"

    const markdown_string = String(readFileSync(markdown_file))
    const mdast = fromMarkdown(markdown_string)

    console.log(JSON.stringify(mdast))
}